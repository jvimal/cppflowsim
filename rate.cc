#ifndef __RATE_H__
#define __RATE_H__

struct Rate {
	double bits_per_ns;
	Rate() {
		bits_per_ns = FLAGS_line_rate;
	}

	Rate(double x) {
		bits_per_ns = x;
	}

	Rate operator-(const Rate &other) {
		Rate r;
		r.bits_per_ns = bits_per_ns - other.bits_per_ns;
		return r;
	}

	bool operator<(const Rate &other) const {
		return bits_per_ns < other.bits_per_ns;
	}

	void operator=(const int &other) {
		bits_per_ns = other;
	}

	Rate operator+=(const Rate &other) {
		bits_per_ns += other.bits_per_ns;
		return *this;
	}

	Rate operator/(const int &other) {
	        Rate r;
		r.bits_per_ns = bits_per_ns / other;
		return r;
	}

	bool Zero() {
		return fabs(bits_per_ns) < 1e-2;
	}

	double Float() {
		return (double)bits_per_ns;
	}
};

#endif /* __RATE_H__ */
