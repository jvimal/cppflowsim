#ifndef __ALLOCATOR_H__
#define __ALLOCATOR_H__

struct Net;

/*
 * TODO(jvimal): The framework should permit per-tenant allocators
 * with any of the sub-allocators: pff, srpt, etc.
 */

struct Allocator {
	Net *net;

	virtual void ComputeRates() {
		printf("Please define a subclass and your allocator there.\n");
		assert(false);
	}
};

struct PerFlowFairAllocator:public Allocator {
	Net *net;
	PerFlowFairAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct FlowSRPTAllocator:public Allocator {
	Net *net;
	FlowSRPTAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct FlowSizePrioAllocator:public Allocator {
	Net *net;
	FlowSizePrioAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct CoFlowSRPTAllocator:public Allocator {
	Net *net;
	CoFlowSRPTAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct CoFlowSizePrioAllocator:public Allocator {
	Net *net;
	CoFlowSizePrioAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct FIFOAllocator:public Allocator {
	Net *net;
	FIFOAllocator(Net *net):net(net) {}
	void ComputeRates();
};

struct PerTenantFairAllocator:public Allocator {
	Net *net;
	int NUM_CLASS;
	PerTenantFairAllocator(Net *net, int num_class):net(net),NUM_CLASS(num_class) {}
	void ComputeRates();
};

struct PerTenantSRPTAllocator:public Allocator {
	Net *net;
	int NUM_CLASS;
	PerTenantSRPTAllocator(Net *net, int num_class):net(net), NUM_CLASS(num_class) {}
	void ComputeRates();
};

struct PerTenantSRPTFairAllocator:public Allocator {
	Net *net;
	int NUM_CLASS;
	PerTenantSRPTFairAllocator(Net *net, int num_class):net(net), NUM_CLASS(num_class) {}
	void ComputeRates();
};

#endif /* __ALLOCATOR_H__ */
