void test_1() {
	CreateTopology(2);
	Flow *f1 = new Flow(0, 3, 100 * 1e4);
	Flow *f2 = new Flow(1, 3, 10 * 1e4);
	flows.push_back(f1);
	flows.push_back(f2);

	TimeUnit t = 0;
	while (flows.size()) {
		ComputePrioRates();
		for (auto f: flows) {
			printf("f %p rate %.3f finish %llu rem %lu size %lu\n",
			       f, f->Rate().Float(), f->FinishTime(), f->remaining, f->size);
		}

		for (auto f: flows) {
			printf("f %p rate %.3f finish %llu rem %lu size %lu\n",
			       f, f->Rate().Float(), f->FinishTime(), f->remaining, f->size);
		}

		TimeUnit next = ComputeFinishTimes();
		printf("finish time %llu\n", next);

		DrainFlows(next);
		FlushFlows();
		t += next;
	}

	for (auto t: fct_stats) {
		printf("FCT ratio: %.3f, %.3fms\n", t.first, t.second);
	}
}
