import re

pat_str = 'App (.*): .(.*). ratio (.*)'
pat_latency = re.compile(pat_str)
pat_flow_avg = re.compile('App (\d): Flows Average: (.*)')
pat_coflow_avg = re.compile('App (\d): CoFlows Average: (.*)')

def parse(fname):
    ret = []
    for line in open(fname).xreadlines():
        line = line.strip()
        m = pat_latency.match(line)
        if m:
            data = ('App %s %.3f latency' % (m.group(1), float(m.group(2))), float(m.group(3)))
            ret.append(data)
        m = pat_flow_avg.match(line)
        if m:
            data = ('App %s avg flow latency' % (m.group(1)), float(m.group(2)))
            ret.append(data)
        m = pat_coflow_avg.match(line)
        if m:
            data = ('App %s avg coflow latency' % (m.group(1)), float(m.group(2)))
            ret.append(data)
    return ret
