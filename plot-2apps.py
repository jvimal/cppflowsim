#!/bin/env python

import argparse
import logging
import os
import datetime
import itertools
import time
import multiprocessing
import matplotlib.pyplot as plt
import sys
import re

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', required=True)
parser.add_argument('--sched', default='./sched')
parser.add_argument('--target_load', type=float, default=0.5)
FLAGS = parser.parse_args()

allocators = ['pff', 'ptf', 'fsrpt', 'csrpt', 'fsizep', 'csizep']
bursts = [1, 2, 4]
TARGET_LOAD=FLAGS.target_load
loads = [0.1, 0.3, 0.5, 0.7, 0.8]
MEM_LIMIT='1000000' # 1GB limit
MAX_PROCS=max(1, multiprocessing.cpu_count()-1)
MAX_FLOWS=1000000
PCILE=99.00

scenarios = itertools.product(allocators, bursts, loads)
pat_str = 'App (\d): .%.3f. ratio (.*)' % PCILE
logging.info("Pattern: %s" % pat_str)
pat_latency = re.compile(pat_str)

def dir(idx, alloc,burst,load):
    return "%03d-alloc-%s--burst-%s--load-%s" % (idx, alloc, burst, load)

if not os.path.exists(FLAGS.dir):
    logging.error("Dir %s does not exist" % FLAGS.dir)
    sys.exit(-1)

idx = -1
scenarios = list(scenarios)
scenarios = filter(lambda (a,b,load): FLAGS.target_load > load, scenarios)

def stat(fname):
    ret = []
    for line in open(fname).xreadlines():
        line = line.strip()
        m = pat_latency.match(line)
        if m:
            ret.append(float(m.group(2)))
    return ret

for alloc,burst,load in scenarios:
    idx += 1
    exptdir = dir(idx, alloc,burst,load)
    outdir = os.path.join(FLAGS.dir, exptdir)
    if not os.path.exists(outdir):
        logging.error("Dir %s does not exist" % outdir)
        continue

    fname = os.path.join(outdir, "log")
    ret = stat(fname)
    ret += [load, burst, alloc]
    print fname, ret
