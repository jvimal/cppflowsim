import pandas as pd
import numpy as np

pd.set_option('display.max_colwidth', 1000)
pd.set_option('display.width', 1000)

METRICS = [
    'App %d avg flow latency',
    'App %d Flows 99.000 latency',
    'App %d Flows 99.900 latency',
    'App %d Flows 99.990 latency',
    'App %d Flows 99.999 latency',
    'App %d avg coflow latency'
    ]

def m(i, j):
    return METRICS[i] % j

class Analysis:
    def __init__(self, df):
        self.df = df
        self.metrics = METRICS

    def pick_min_stat(self, df, colname):
        ret = df[df[colname] == min(df[colname].values)]
        assert(len(ret.values) == 1)
        return ret

    def pick_nth_min_stat(self, df, colname, n):
        vals = list(np.unique(df[colname].values))
        vals.sort()
        ret = df[df[colname] == vals[n]]
        nth_bestval = vals[n]
        #print ret.values
        nth_bestsched = '+'.join(ret.values.T[0])
        return nth_bestsched, nth_bestval

    def pick_best_sched_for_app(self, app_id, metric, nth_best=0):
        metric = metric % app_id
        df = self.df
        sel = df[(df.stat == metric)]
        load = 'load%d' % app_id
        otherload = 'load%d' % (1-app_id)
        g = sel.groupby([load, otherload])
        ret = []
        retcolumns = [load, otherload, 'value']
        for name, group in g:
            dfmin = self.pick_min_stat(group, 'value')
            best = dfmin['alloc'].values[0]
            bestval = dfmin['value'].values[0]
            blah = []
            for i in range(0, nth_best+1):
                nth_bestsched, nth_bestval = self.pick_nth_min_stat(group, 'value', i)
                blah.append(('%6s' % nth_bestsched, '%6.2f' % nth_bestval))
            ret.append((name[0], name[1], tuple(blah)))
        dfret = pd.DataFrame(ret, columns=retcolumns)
        return dfret


def merge(df0, df1):
    return df0.merge(df1, on=['load0', 'load1'], suffixes=['_app0', '_app1'])

def analyse(df, m0, m1, nth_best=0):
    a = Analysis(df)
    print '******* Comparing %s and %s' % (a.metrics[m0] % 0, a.metrics[m1] % 1)
    dfret0 = a.pick_best_sched_for_app(0, a.metrics[m0], nth_best=nth_best)
    dfret1 = a.pick_best_sched_for_app(1, a.metrics[m1], nth_best=nth_best)
    return merge(dfret0, dfret1)
