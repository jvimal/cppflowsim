#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdarg.h>
#include <string>
using namespace std;

#define LET(it, x) typeof(x) it(x)
#define EACH(it, cont) for (LET(it, (cont).begin()); it != (cont).end(); ++it)
#define REP(i, n) for (int i = 0; i < n; ++i)

typedef unsigned long long u64;
typedef unsigned long u32;
typedef unsigned char u8;

typedef u64 TimeUnit;
const TimeUnit ONE_SECOND = 1000 * 1000LLU * 1000LLU;

struct FCTStat {
	TimeUnit fct;
	TimeUnit ideal;
	double ratio;
	u32 size;

	FCTStat(){}

	FCTStat(TimeUnit f, TimeUnit i, u32 s) {
		fct = f;
		ideal = i;
		size = s;
		// ratio = max(1.0, fct*1.0/ideal);
		ratio = fct*1.0/ideal;
	}

};

inline bool CompareFCT(const FCTStat &a, const FCTStat &b) {
	return a.ratio < b.ratio;
}

string Sprintf(const char *fmt, ...) {
	char buff[1024];
	va_list(ap);
	va_start(ap, fmt);
	vsprintf(buff, fmt, ap);
	va_end(ap);
	return string(buff);
}

vector<string> StringSplit(const string& str, string delimiter)
{
	vector<string> ret;
	size_t pos = 0;

	while (str.substr(pos).find(delimiter) != string::npos) {
		auto it = str.substr(pos).find(delimiter);
		ret.push_back(str.substr(pos, it));
		pos += it + delimiter.size();
	}

	ret.push_back(str.substr(pos));

	return ret;
}

#endif /* __COMMON_H__ */
