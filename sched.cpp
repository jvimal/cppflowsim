#include <queue>
#include <list>
#include <cassert>
#include <string>
#include <algorithm>
using namespace std;
#include <time.h>
#include <stdio.h>
#include "random.h"
#include "common.h"
#include <stdlib.h>

#include <glog/logging.h>
#include <gflags/gflags.h>

DEFINE_int32(num_nodes, 30, "Number of nodes in the bipartite network.");
DEFINE_int32(num_apps, 1, "Number of apps per node (default 1, max 2).");
DEFINE_int32(burst_flow_arrivals, 2, "Burstiness of flow arrivals.");
DEFINE_double(mean_flow_size, 1e6, "Mean flow size.");
DEFINE_string(loads, "0.3,0.3", "Comma separated value for loads of two tenants.");
DEFINE_double(line_rate, 10, "Line rate in Gb/s.");
DEFINE_string(allocator, "pff",
	      "Rate allocator.  pff is per-flow-fair, ptf is per-tenant-fair, "
	      "ptsrpt is per-tenant srpt, "
	      "{f,c}srpt is the greedy srpt across fabric across flows (f) and coflows (c), "
	      "{f,c}sizep is size-based prio across flows (f) and coflows (c).");
DEFINE_int32(max_flows, 100000,
	     "Maximum number of flows to run before stopping");
DEFINE_bool(all_stats, false,
	    "Print all the percentiles when showing FCT");
DEFINE_int32(seed, 0,
	     "Random number seed");

// double mean_inter = 1e-3;
double load0 = 0.5;
double load1 = 0.25;

u64 TIME_INFINITY = ~0;
u64 NANOSEC = 1;
u64 SECOND = 1e9;
const int MAX_NUM_NODES = 400;

#include "rate.cc"
#include "link.cc"
#include "flow.cc"
#include "app.cc"
#include "net.cc"

int main(int argc, char *argv[]) {
	google::InitGoogleLogging(argv[0]);
	google::ParseCommandLineFlags(&argc, &argv, true);
	FLAGS_logtostderr = true;
	rand_init(FLAGS_seed);
	double mean_inter, mean_inter0, mean_inter1;

	vector<string> loads = StringSplit(FLAGS_loads, ",");
	load0 = atof(loads[0].c_str());
	load1 = atof(loads[1].c_str());

	mean_inter = (FLAGS_mean_flow_size * 8 / FLAGS_line_rate) * NANOSEC; /* nanoseconds */

	LOG(INFO) << Sprintf("Mean size: %.3fMB, mean inter: %.3f ns\n",
			     FLAGS_mean_flow_size/1e6, mean_inter);
	LOG(INFO) << Sprintf("Line rate: %.3fGb/s, Load: %.3f, %.3f\n",
			     FLAGS_line_rate, load0, load1);

	App *app = new App(0, FLAGS_num_nodes, FLAGS_burst_flow_arrivals);
	app->sizes = new RandomPareto(FLAGS_mean_flow_size, 1.1);
	app->sizes = new RandomExp(FLAGS_mean_flow_size);
	mean_inter0 = (FLAGS_mean_flow_size * 8 / FLAGS_line_rate) * NANOSEC; /* nanoseconds */
	app->inter = new RandomPareto(mean_inter0/load0, 1.12);
	app->inter = new RandomExp(mean_inter0/load0);

	App *app2 = new App(1, FLAGS_num_nodes, FLAGS_burst_flow_arrivals);
	app2->sizes = new RandomPareto(FLAGS_mean_flow_size*2, 1.1);
	app2->sizes = new RandomExp(FLAGS_mean_flow_size*2);
	mean_inter1 = (FLAGS_mean_flow_size*2 * 8 / FLAGS_line_rate) * NANOSEC; /* nanoseconds */
	app2->inter = new RandomPareto(mean_inter1/load1, 1.1);
	app2->inter = new RandomExp(mean_inter1/load1);

	Net *net = new Net;
	if (load0 > 0)
		net->AddApp(app);

	if (FLAGS_num_apps == 2 and load1 > 0)
		net->AddApp(app2);

	net->CreateTopology(FLAGS_num_nodes);

	if (FLAGS_allocator == "pff") {
		net->allocator = new PerFlowFairAllocator(net);
	} else if (FLAGS_allocator == "ptf") {
		net->allocator = new PerTenantFairAllocator(net, FLAGS_num_apps);
	} else if (FLAGS_allocator == "fsrpt") {
		net->allocator = new FlowSRPTAllocator(net);
	} else if (FLAGS_allocator == "fsizep") {
		net->allocator = new FlowSizePrioAllocator(net);
	} else if (FLAGS_allocator == "csrpt") {
		net->allocator = new CoFlowSRPTAllocator(net);
	} else if (FLAGS_allocator == "csizep") {
		net->allocator = new CoFlowSizePrioAllocator(net);
	} else if (FLAGS_allocator == "ptsrpt") {
		net->allocator = new PerTenantSRPTAllocator(net, FLAGS_num_apps);
	} else if (FLAGS_allocator == "ptfsrpt") {
		net->allocator = new PerTenantSRPTFairAllocator(net, FLAGS_num_apps);
	} else {
		LOG(INFO) << "Invalid allocator";
		exit(-1);
	}

	TimeUnit t = 0;
	TimeUnit end = 10 * SECOND;
	TimeUnit next_arrival = 0;
	TimeUnit avg_step = 1;

	clock_t start_time = clock();
	clock_t next_print = clock();
	LOG(INFO) << Sprintf("Time: %.3fs\n", end/1e9);

	while (net->TotalFlows() < FLAGS_max_flows) {
		if (clock() >= next_print) {
			next_print += CLOCKS_PER_SEC;
			LOG(INFO) << Sprintf("[%12llu nsec, %3.2f%%, %4lu live flows, %4u total flows, %6llu nsec avg step]\n",
					     t, (net->TotalFlows()*100.0/FLAGS_max_flows),
					     net->flows.size(), net->TotalFlows(), avg_step);
			LOG(INFO) << Sprintf("  Rate at node 1: tx %.3lf Gb/s, rx %.3lf Gb/s\n",
					     net->txrate[0].Gbps(), net->rxrate[0].Gbps());

			CHECK_NE(avg_step, 0) << Sprintf("Simulation stalled?");
		}

		/* Process arrivals */
		while (t == next_arrival) {
			int loop = 0;
			Flow *f = net->PickEarliest(t);
			net->AddFlow(f);
			next_arrival = net->NextEarliestArrival();
		}

		/* Process any timeouts */

		net->ComputeRates();

		/* The earliest time when one flow finishes in the
		 * system */
		TimeUnit next_finish_dt = net->ComputeFinishTimes();
		TimeUnit dt = min(next_finish_dt, next_arrival - t);

		/* Clock the system until this time instant */
		net->DrainFlows(t, dt);
		net->FlushFlows(t+dt);

		t += dt;
		avg_step = (avg_step) * 0.95 + 0.05 * dt;
	}

	clock_t finish_time = clock();
	app->PrintStats();
	app2->PrintStats();
	LOG(INFO) << Sprintf("Took %.3f seconds\n",
			     (finish_time - start_time)*1.0/CLOCKS_PER_SEC);
	return 0;
}
