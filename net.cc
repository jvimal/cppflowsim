#ifndef __NET_CC__
#define __NET_CC__

#include <vector>
using namespace std;

#include "app.cc"
#include "allocator.cc"
#include "net.h"

void Net::CreateTopology(int num_nodes) {
	this->num_nodes = num_nodes;
	REP(i, num_nodes * 2) {
		Link *l = new Link;
		links.push_back(l);
	}
}

void Net::AddApp(App *app) {
	apps.push_back(app);
	app->SetNet(this);
}

void Net::ComputeRates() {
	allocator->ComputeRates();
}

void Net::NotifyFlowCompletion(Flow *f, TimeUnit now) {
	total_flows++;
}

Flow *Net::PickEarliest(TimeUnit t) {
	Flow *ret = NULL;
	App *rapp = NULL;

	for (auto app: apps) {
		Flow *f = app->PickEarliest(t);
		if (ret == NULL) {
			ret = f;
			rapp = app;
		} else if (f->StartTime() < ret->StartTime()) {
			ret = f;
			rapp = app;
		}
	}

	if (ret != NULL)
		rapp->RemoveFlow(ret);
	return ret;
}

TimeUnit Net::NextEarliestArrival() {
	TimeUnit ret = TIME_INFINITY;
	for (auto app: apps)
		ret = min(ret, app->EarliestArrival());
	return ret;
}

TimeUnit Net::ComputeFinishTimes() {
	TimeUnit next = TIME_INFINITY;
	for (auto f: flows) {
		next = min(next, f->TimeToFinish());
	}
	return next;
}

void Net::DrainFlows(TimeUnit now, TimeUnit dt) {
	for (auto f: flows) {
		u32 bytes = f->Drain(dt);
		txrate[f->src].AddBytes(bytes, now);
		rxrate[f->dst-num_nodes].AddBytes(bytes, now);
	}
}

void Net::FlushFlows(TimeUnit now) {
	list<Flow *> done;
	for (auto f: flows) {
		if (f->Completed()) {
			f->NotifyDone(now);
			this->apps[f->klass]->NotifyFlowCompletion(f);
			this->NotifyFlowCompletion(f, now);
			done.push_back(f);
		}
	}

	for (auto f: done) {
		flows.erase(find(flows.begin(), flows.end(), f));
		f->slink->RemoveFlow(f);
		f->dlink->RemoveFlow(f);
		delete f;
	}
}

void Net::AddFlow(Flow *f) {
	this->flows.push_back(f);
}

#endif /* __NET_CC__ */
