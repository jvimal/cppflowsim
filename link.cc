#ifndef __LINK_H__
#define __LINK_H__

#include <unordered_map>
using namespace std;

#include "flow.h"
#include <strings.h>
#include <string.h>

#define MAX_NUM_CLASS 3

struct Link {
	Rate capacity;
	Rate capacity_class[MAX_NUM_CLASS];
	Rate allocated;
	Rate allocated_class[MAX_NUM_CLASS];

	vector<Flow *> flows;
	TimeUnit last;
	int active_flows;
	//unordered_map<int,int> num_flows;
	//unordered_map<int,int> num_active_flows;
	int num_flows[MAX_NUM_CLASS];
	int num_active_flows[MAX_NUM_CLASS];

	Link() {
		capacity = FLAGS_line_rate;
		allocated = 0;
		last = 0;
		bzero(num_flows, sizeof(num_flows));
		bzero(num_active_flows, sizeof(num_active_flows));
		bzero(capacity_class, sizeof(capacity_class));
		bzero(allocated_class, sizeof(allocated_class));
	}

	Rate GetAvailable(int klass=-1) {
		if (klass == -1) {
			return capacity - allocated;
		} else {
			return capacity_class[klass] - allocated_class[klass];
		}
	}

	void ResetActive() {
		active_flows = flows.size();
		//num_active_flows = num_flows;
		memcpy(num_active_flows, num_flows, sizeof(num_active_flows));
		allocated = 0;
		memset(allocated_class, 0, sizeof(allocated_class));
	}

	void DecActive(int klass=-1) {
		active_flows--;
		assert(active_flows >= 0);
		if (klass != -1) {
			int &n = num_active_flows[klass];
			n--;
			assert(n>=0);
		}
	}

	int NumActiveFlows(int klass=-1) {
		if (active_flows > 10000)
			printf("Error>:  too many active flows... %d\n", active_flows);
		if (klass == -1)
			return active_flows;
		return num_active_flows[klass];
	}

	void AddCapacity(int klass, Rate r) {
		capacity_class[klass] += r;
	}

	void SetCapacity(int klass, Rate r) {
		capacity_class[klass] = r;
	}

	void Consume(Rate r, int klass=-1) {
		allocated += r;
		if (klass != -1) {
			allocated_class[klass] += r;
		}
	}

	bool Full(int klass=-1) {
		if (klass == -1) {
			Rate r = capacity - allocated;
			return r.Zero();
		} else {
			Rate r = capacity_class[klass] - allocated_class[klass];
			return r.Zero();
		}
	}

	void RemoveFlow(Flow *f) {
		auto it = find(flows.begin(), flows.end(), f);
		num_flows[f->klass]--;
		flows.erase(it);
	}

	void AddFlow(Flow *f) {
		num_flows[f->klass]++;
		flows.push_back(f);
	}
};

#endif /* __LINK_H__ */
