#ifndef __FLOW_CC__
#define __FLOW_CC__

#include <vector>
using namespace std;
#include "net.h"
#include "flow.h"

struct Flow;

vector<FCTStat> coflow_fcts;

inline bool CompareFlows(const Flow *a, const Flow *b) {
	return a->remaining < b->remaining;
}

inline bool CompareFlowSizes(const Flow *a, const Flow *b) {
	return a->size < b->size;
}

inline bool CompareCoFlowSizes(const Flow *a, const Flow *b) {
	if (a->coflow && b->coflow) {
		return a->coflow->total_size < b->coflow->total_size;
	}
	LOG(WARNING) << "No coflows to compare";
	return false;
}

inline bool CompareCoFlowRemainingSizes(const Flow *a, const Flow *b) {
	if (a->coflow && b->coflow) {
		return a->coflow->remaining_size < b->coflow->remaining_size;
	}
	LOG(WARNING) << "No coflows to compare";
	return false;
}

Flow::Flow(int src, int dst, u32 size, Net *net, int klass) {
	this->src = src;
	this->dst = dst;
	this->size = size;
	this->remaining = size;
	this->slink = net->links[src];
	this->dlink = net->links[dst];
	this->klass = klass;
	this->slink->AddFlow(this);
	this->dlink->AddFlow(this);
	this->completion_time = 0;
	this->coflow = NULL;
}


void CoFlow::AddFlow(Flow *f) {
	flows.push_back(f);
	f->coflow = this;
	this->ideal += f->GetIdeal(FLAGS_line_rate);
	this->total_size += f->size;
	this->remaining_size += f->size;
}

#endif /* __FLOW_CC__ */
