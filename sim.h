#ifndef __SIM_H__
#define __SIM_H__

#include <set>
using namespace std;
#include "common.h"

typedef u64 TimeUnit;

struct Sim;

struct Event {
	TimeUnit t;
	Sim *sim;
	bool inq;
	bool canceled;

	Event():inq(false), canceled(false) { }
	virtual void run() = 0;
	virtual const char *n() = 0;
};

struct EventPtrCompare: public std::binary_function<Event*, Event*, bool> {
	bool operator() (const Event *lhs, const Event *rhs) const {
		return lhs->t > rhs->t;
	}
};

struct Sim {
	set<Event *, EventPtrCompare> eq;
	TimeUnit now;

	Sim():now(0) {}

	void Init() {
		now = 0;
		while (!eq.empty()) {
			auto it = eq.begin();
			eq.erase(it);
			delete *it;
		}
	}

	void Schedule(Event *e, TimeUnit t) {
		if (e->inq)
			return;
		e->t = now + t;
		e->sim = this;
		e->inq = true;
		eq.insert(e);
	}

	void CancelEvent(Event *e) {
		auto it = eq.find(e);
		if (it != eq.end())
			eq.erase(it);
	}

	void Run(TimeUnit T) {
		TimeUnit t;

		while (!eq.empty()) {
			auto it = eq.begin();
			eq.erase(it);
			Event *e = *it;
			if (e->t > T)
				break;

			assert(e->t >= now);
			now = e->t;
			e->inq = false;
			e->run();
		}

		// LOG(INFO) << now << " Simulation finishing";
	}
};



#endif /* __SIM_H__ */


