#!/bin/env python

import argparse
import logging
import os
import datetime
import subprocess
import itertools
import time
import multiprocessing
import re
import pprint
import pandas as pd
import simparser

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', default='out/' + datetime.datetime.now().strftime('%b-%H-%M'),)
parser.add_argument('--sched', default='./sched')
parser.add_argument('--summarise', default=False, action='store_true')

FLAGS = parser.parse_args()

allocators = ['pff', 'fsrpt', 'csrpt', 'fsizep', 'csizep']
bursts = [1, 2, 4]
loads = [0.1, 0.3, 0.5, 0.7, 0.9]

MEM_LIMIT='1000000' # 1GB limit
MAX_PROCS=max(1, multiprocessing.cpu_count()-1)
MAX_FLOWS=1000000

scenarios = itertools.product(allocators, bursts, loads)
procs = []
doneprocs = []

def dir(idx, alloc,burst,load):
    return "%03d-alloc-%s--burst-%s--load-%s" % (idx, alloc, burst, load)

def start(alloc,burst,load,outfile):
    cmd = "%s -allocator %s -max_flows %s -burst_flow_arrivals %s -loads %s,%s"
    cmd = cmd % (FLAGS.sched, alloc, MAX_FLOWS, burst, load, load)
    cmd = "(%s >%s 2>&1)" % (cmd, outfile)
    limit = "ulimit -Sv %s" % MEM_LIMIT
    proc = subprocess.Popen("%s; %s;" % (limit, cmd), shell=True)
    logging.info("Started %s" % cmd)
    procs.append(proc)

    f = open(os.path.dirname(outfile) + "/cmd", 'w')
    f.write(cmd)
    f.close()
    return proc

def wait(all=False):
    global procs, doneprocs
    while True:
        if all is False and len(procs) != MAX_PROCS:
            break
        if all is True and len(procs) == 0:
            break
        time.sleep(1)
        keep = []
        for p in procs:
            if p.poll() is None:
                keep.append(p)
            else:
                doneprocs.append(p)
        procs = keep
    return

if not os.path.exists(FLAGS.dir):
    os.makedirs(FLAGS.dir)


def plot_graphs(df):
    # One file each to plot x:load, y:fct-ratio for all scheduling
    # algorithms, for each statistic and for each burst value.
    import matplotlib.pyplot as plt
    stats = set(df['stat'])
    bursts = set(df['burst'])
    scheds = set(df['sched'])
    loads = set(df['load'])
    loads = list(loads)
    loads.sort()
    for stat in stats:
        for burst in bursts:
            plt.figure()
            fname = 'burst-%s--stat-%s.png' % (burst, stat.lower().replace(' ','-'))
            fname = os.path.join(FLAGS.dir, fname)
            for sched in scheds:
                xs = loads
                ys = df[(df.stat == stat) & (df.burst == burst) & (df.sched == sched)]['value']
                plt.plot(xs, ys, label=sched, lw=2)
            plt.legend(loc='upper left')
            plt.grid(True)
            plt.title(stat)
            plt.savefig(fname)
            logging.info("Saving %s" % fname)

if FLAGS.summarise == False:
    idx = -1
    scenarios = list(scenarios)
    start = time.time()
    for alloc,burst,load in scenarios:
        idx += 1
        wait()
        exptdir = dir(idx, alloc,burst,load)
        outdir = os.path.join(FLAGS.dir, exptdir)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        outfile = os.path.join(outdir, "log")
        start(alloc,burst,load,outfile)
        N = len(scenarios)
        M = len(doneprocs)
        logging.info("Done %d/%d [%.2f%%]" % (M, N, M*100.0/N))

    wait(all=True)
    end = time.time()
    logging.info("Sweep took %s seconds" % (end-start))
else:
    scenarios = list(scenarios)
    lst = []
    for idx,(alloc,burst,load) in enumerate(scenarios):
        exptdir = dir(idx, alloc,burst,load)
        outdir = os.path.join(FLAGS.dir, exptdir)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        outfile = os.path.join(outdir, "log")
        stats = simparser.parse(outfile)
        pprint.pprint(outfile)
        pprint.pprint(stats)
        for k,v in stats:
            lst.append((alloc, burst, load, k, v))
        print '--------------'

    data = pd.DataFrame(lst,
                        columns=['sched', 'burst', 'load', 'stat', 'value'])
    print data
    plot_graphs(data)
    import IPython
    IPython.embed()
