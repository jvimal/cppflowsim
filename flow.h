#ifndef __FLOW_H__
#define __FLOW_H__

#include "rate.cc"

struct Link;
struct CoFlow;
struct Net;
struct Flow;

struct CoFlow {
	list<Flow *> flows;
	TimeUnit start, finish;
	TimeUnit completion_time;
	TimeUnit ideal;
	u32 total_size;
	u32 remaining_size;
	bool done;
	FCTStat stat;

	CoFlow(TimeUnit t) {
		start = t;
		done = false;
		ideal = 0;
		total_size=0;
		remaining_size=0;
		stat.ratio = 0;
	}

	void SetStart(TimeUnit t) { start = t; }

	void AddFlow(Flow *);

	void RemoveFlow(Flow *f) {
		auto it = find(flows.begin(), flows.end(), f);
		if (it != flows.end())
			flows.erase(it);
	}

	void NotifyFlowCompletion(Flow *f, TimeUnit now) {
		RemoveFlow(f);
		if (flows.size() == 0) {
			TimeUnit fct = now - start;
			finish = now;
			stat = FCTStat(fct, ideal, total_size);
			/* TODO: this bug happens for the first flow... */
			if (finish < start)
				stat.ratio = 0;
			done = true;
		}
	}

	bool Done() {
		return done;
	}

	void Drain(u32 bytes, TimeUnit dt) {
		remaining_size -= bytes;
		completion_time += dt;
	}
};

struct Flow {
	u32 size;
	u32 remaining;
	int src, dst;
	/* IF we are doing fair queueing, etc. */
	int klass;

	TimeUnit start, finish;
	TimeUnit completion_time;

	Rate r;
	Rate allocated;

	Link *slink, *dlink;
	bool bottlenecked;

	CoFlow *coflow;

	Flow(int,int,u32, Net*, int id);

	TimeUnit StartTime() {
		return start;
	}

	void SetStartTime(TimeUnit t) {
		start = t;
	}

	TimeUnit TimeToFinish() {
		if (allocated.Zero())
			return TIME_INFINITY;
		return remaining * 8 / (allocated.bits_per_ns) * NANOSEC;
	}

	void Reset() {
		r = 0;
		allocated = 0;
		bottlenecked = false;
	}

	u32 Drain(TimeUnit dt) {
		int bytes = dt * allocated.Float() / 8.0;
		remaining -= bytes;
		completion_time += dt;
		if (coflow) {
			coflow->Drain(bytes, dt);
		}
		return bytes;
	}

	bool Done() {
		return bottlenecked;
	}

	bool Completed() {
		return remaining < 2;
	}

	TimeUnit GetFCT() {
		return completion_time;
	}

	TimeUnit GetIdeal(double linerate) {
		return size * 8.0 / (linerate / FLAGS_num_apps);
	}

	void Bottlenecked() {
		bottlenecked = true;
	}

	Rate Rate() {
		return allocated;
	}

	void NotifyDone(TimeUnit now) {
		/* Called when the flow is done */
		if (coflow) {
			coflow->NotifyFlowCompletion(this, now);
		}
	}
};

#endif /* __FLOW_H__ */
