#!/bin/bash

function glog {
	svn checkout http://google-glog.googlecode.com/svn/trunk/ google-glog-read-only
	pushd google-glog-read-only
	./configure && make && make install
	popd
}

function gflags {
	git clone https://github.com/schuhschuh/gflags
	pushd gflags
	./configure && make && make install
	popd
}

glog
gflags

