#ifndef __RATE_EST_H__
#define __RATE_EST_H__

#include "common.h"

struct RateEstimator {
	u64 bytes;
	u64 last_bytes;
	TimeUnit last_time;
	double est;
	double gain;

	RateEstimator() {
		bytes = 0;
		last_bytes = 0;
		last_time = 0;
		est = 0;
		gain = 1.0/8;
	}

	void AddBytes(u64 bytes, TimeUnit now) {
		this->bytes += bytes;
		Update(now);
	}

	void Update(TimeUnit now) {
		if ((now - last_time) < ONE_SECOND)
			return;

		double dt = (now - last_time) * 1.0/ONE_SECOND;
		double rate = (bytes-last_bytes) * 8.8 / dt;
		est = est * (1.0-gain) + rate * gain;

		last_bytes = bytes;
		last_time = now;
	}

	double Gbps() {
		return est / 1e9;
	}
};

#endif /* __RATE_EST_H__ */
