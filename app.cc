#ifndef __APP_CC__
#define __APP_CC__

#include <list>
using namespace std;
#include "random.h"
#include "net.h"
#include "common.h"

struct App
{
	list<Flow *> next_arrivals[MAX_NUM_NODES];
	list<CoFlow *> coflows;

	Random *inter;
	Random *sizes;
	Random *dst_ru;

	int id;
	int num_nodes;
	int burst_flow_arrivals;

	Net *net;
	vector<FCTStat> fctstats;
	vector<FCTStat> coflowfctstats;

	App() { id = 0; num_nodes = 0; burst_flow_arrivals = 0; }

	App(int id, int num_nodes, int burst):id(id),
					      num_nodes(num_nodes),
					      burst_flow_arrivals(burst) {
		dst_ru = new RandomUniform(num_nodes, 2*num_nodes);
		//sizes = new RandomPareto(FLAGS_mean_flow_size, 1.1);
		//inter = new RandomExp(mean_inter);
		sizes = NULL;
		inter = NULL;
		net = NULL;
	}

	void NotifyFlowCompletion(Flow *f) {
		assert(f->Completed());
		TimeUnit t = f->GetFCT();
		TimeUnit ideal = f->GetIdeal(FLAGS_line_rate);

		/* Some corner cases as we are not precise
		 * about FCTs.  So this can happen. */
		// t = max(ideal, t);

		if (t == 0)
			return;

		FCTStat stat = FCTStat(t, ideal, f->size);
		fctstats.push_back(stat);

		if (f->coflow and f->coflow->Done()) {
			coflowfctstats.push_back(f->coflow->stat);
			delete f->coflow;
			f->coflow = NULL;
		}
	}

	void PrintFCTStats(vector<FCTStat> &fctstats, string msg) {
		if (fctstats.size() == 0)
			return;
		sort(fctstats.begin(), fctstats.end(), CompareFCT);
		int L = fctstats.size();
		double sum = 0;
		int num_counted = 0;

		for (auto t: fctstats) {
			if (t.ratio < 1e10) {
				sum += t.ratio;
				num_counted++;
			}
		}
		printf("App %d: %s Average: %.3f\n", id, msg.c_str(), sum/num_counted);

		if (FLAGS_all_stats) {
			for (int i = 10; i < 100; i += 10) {
				auto t = fctstats[min(L-1, int(i * L / 100.0))];
				printf("App %d %s: [%d] ratio %f\n", id, msg.c_str(), i, t.ratio);
			}
		}


		float arr[] = {0.99, 0.999, 0.9999, 0.99999};
		for (int i = 0; i < 4; i++) {
			auto t = fctstats[int(arr[i] * L)];
			printf("App %d %s: [%.3f] ratio %f\n", id, msg.c_str(), arr[i]*100, t.ratio);
		}
	}


	void PrintStats() {
		printf("App %d: Flows: %lu, Coflows: %lu\n", id, fctstats.size(), coflowfctstats.size());
		PrintFCTStats(fctstats, "Flows");
		PrintFCTStats(coflowfctstats, "CoFlows");
	}

	void SetNet(Net *net) {
		this->net = net;
	}

	void NextBatchArrivals(TimeUnit now);
	bool NextArrivalsEmpty() {
		REP(i, num_nodes)
			if (next_arrivals[i].empty())
				return true;
		return false;
	}

	Flow *PickEarliest(TimeUnit now) {
		if (NextArrivalsEmpty())
			NextBatchArrivals(now);

		Flow *ret = *next_arrivals[0].begin();
		for (int i = 1; i < num_nodes; i++) {
			Flow *f = *next_arrivals[i].begin();
			if (f->StartTime() < ret->StartTime())
				ret = f;
		}

		return ret;
	}

	void RemoveFlow(Flow *ret) {
		/* Remove the flow */
		REP(i, num_nodes) {
			if (*next_arrivals[i].begin() == ret) {
				next_arrivals[i].erase(next_arrivals[i].begin());
				break;
			}
		}
	}

	int PickDestination() {
		return dst_ru->gen();
	}

	TimeUnit EarliestArrival() {
		Flow *ret = NULL;
		for (int i = 0; i < num_nodes; i++) {
			if (next_arrivals[i].empty())
				continue;
			Flow *f = *next_arrivals[i].begin();
			if (ret == NULL) {
				ret = f;
			} else {
				if (f->StartTime() < ret->StartTime())
					ret = f;
			}
		}
		return ret->StartTime();
	}
};

void App::NextBatchArrivals(TimeUnit now)
{
	REP(src, num_nodes) {
		TimeUnit t = inter->gen() * burst_flow_arrivals;

		if (!next_arrivals[src].empty())
			continue;

		t += now;

		CoFlow *cf = new CoFlow(t);
		coflows.push_back(cf);

		REP(j, burst_flow_arrivals) {
			int dst = PickDestination();
			Flow *f = new Flow(src, dst, sizes->gen(), net, id);
			f->klass = this->id;
			f->SetStartTime(t);
			cf->AddFlow(f);
			next_arrivals[src].push_back(f);
		}
	}
}


#endif /* __APP_CC__ */
