#!/bin/env python

import argparse
import logging
import os
import datetime
import subprocess
import itertools
import time
import multiprocessing
import numpy as np
import pandas as pd
import simparser
import pprint
from analysis import analyse

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', default='out/' + datetime.datetime.now().strftime('%b%d-%H-%M'),)
parser.add_argument('--sched', default='./sched')
parser.add_argument('--target_load', type=float, default=0.5)
parser.add_argument('--dry-run', default=False, action="store_true")
parser.add_argument('--summarise', default=False, action="store_true")

FLAGS = parser.parse_args()

allocators = ['pff', 'ptf', 'fsrpt', 'ptsrpt']
#bursts = [1, 2, 4]
bursts=[1]
TARGET_LOAD=FLAGS.target_load

loads = np.arange(0.0, 1.0, 0.1)
loads = filter(lambda (l0,l1): l0+l1 <= FLAGS.target_load and l0+l1 >= 0,
               itertools.product(loads, loads))

MEM_LIMIT='1000000' # 1GB limit
MAX_PROCS=max(1, multiprocessing.cpu_count()-1)
MAX_FLOWS=1000000

scenarios = itertools.product(allocators, bursts, loads)
procs = []
doneprocs = []

def dir(idx, alloc, burst, load0, load1):
    return "%03d-alloc-%s--burst-%s--load-%s-%s" % (idx, alloc, burst, load0, load1)

def start(alloc,burst,load0,load1,outfile):
    cmd = "%s -allocator %s -max_flows %s -burst_flow_arrivals %s -loads %s,%s -num_apps 2"
    cmd = cmd % (FLAGS.sched, alloc, MAX_FLOWS, burst, load0, load1)
    cmd = "(%s >%s 2>&1)" % (cmd, outfile)
    limit = "ulimit -Sv %s" % MEM_LIMIT

    if FLAGS.dry_run:
        logging.info(cmd)
        proc = None
    else:
        proc = subprocess.Popen("%s; %s;" % (limit, cmd), shell=True)
        logging.info("Started %s" % cmd)
        procs.append(proc)

        f = open(os.path.dirname(outfile) + "/cmd", 'w')
        f.write(cmd)
        f.close()
    return proc

def wait(all=False):
    global procs, doneprocs
    if FLAGS.dry_run:
        return
    while True:
        if all is False and len(procs) != MAX_PROCS:
            break
        if all is True and len(procs) == 0:
            break
        time.sleep(1)
        keep = []
        for p in procs:
            if p.poll() is None:
                keep.append(p)
            else:
                doneprocs.append(p)
        procs = keep
    return

if not os.path.exists(FLAGS.dir):
    os.makedirs(FLAGS.dir)

scenarios = list(scenarios)

if not FLAGS.summarise:
    for idx,(alloc,burst,load) in enumerate(scenarios):
        wait()
        exptdir = dir(idx, alloc, burst, load[0], load[1])
        outdir = os.path.join(FLAGS.dir, exptdir)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        outfile = os.path.join(outdir, "log")
        start(alloc,burst,load[0],load[1],outfile)
        N = len(scenarios)
        M = len(doneprocs)
        logging.info("Done %d/%d [%.2f%%]" % (M, N, M*100.0/N))
    wait(all=True)
else:
    results = []
    columns = ["alloc", "burst", "load0", "load1", "stat", "value"]
    for idx,(alloc,burst,load) in enumerate(scenarios):
        exptdir = dir(idx, alloc, burst, load[0], load[1])
        outdir = os.path.join(FLAGS.dir, exptdir)
        if not os.path.exists(outdir):
            logging.error("outdir %s doesn't exist" % outdir)
            continue
        outfile = os.path.join(outdir, "log")
        try:
            data = simparser.parse(outfile)
        except:
            logging.error("parsing error in %s" % outfile)
            continue
        pprint.pprint(data)
        for k,v in data:
            results.append((alloc, burst, load[0], load[1], k, v))
    print len(results)
    df = pd.DataFrame(results, columns=columns)
    df = list(df.groupby('burst'))[0][1]
    dfret = analyse(df, 0, -1)

    import IPython
    IPython.embed()
