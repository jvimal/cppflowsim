#ifndef __ALLOCATOR_CC__
#define __ALLOCATOR_CC__

#include <list>
using namespace std;

#include "rate.cc"
#include "net.h"
#include "flow.cc"
#include "link.cc"
#include "allocator.h"

void PerFlowFairAllocator::ComputeRates() {
	int num_allocated = 0;
	list<Flow *> lflows;

	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows) {
		flow->Reset();
		lflows.push_back(flow);
	}

	while (num_allocated < net->flows.size()) {
		for (auto f: lflows) {
			if (f->Done())
				continue;

			Rate r = min(f->slink->GetAvailable() / f->slink->NumActiveFlows(),
				     f->dlink->GetAvailable() / f->dlink->NumActiveFlows());

			f->r = r;
			f->allocated += r;
		}

		for (auto f: lflows) {
			if (f->Done())
				continue;

			f->slink->Consume(f->r);
			f->dlink->Consume(f->r);
		}

		for (auto it = lflows.begin(); ; ) {
			if (it == lflows.end())
				break;

			Flow *f = *it;
			if (f->Done()) {
				/* This shouldn't happen */
				it = lflows.erase(it);
				continue;
			}

			if (f->slink->Full() || f->dlink->Full()) {
				f->slink->DecActive();
				f->dlink->DecActive();
				num_allocated++;
				f->Bottlenecked();
				it = lflows.erase(it);
			} else {
				it++;
			}
		}
	}
}

void FlowSRPTAllocator::ComputeRates() {
	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows) {
		flow->Reset();
	}

	sort(net->flows.begin(), net->flows.end(), CompareFlows);

	for (auto f: net->flows) {
		if (f->slink->Full() || f->dlink->Full())
			continue;
		f->r = Rate(FLAGS_line_rate);
		f->allocated += f->r;
		f->slink->Consume(f->r);
		f->dlink->Consume(f->r);
	}
}

void FlowSizePrioAllocator::ComputeRates() {
	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows)
		flow->Reset();

	sort(net->flows.begin(), net->flows.end(), CompareFlowSizes);

	for (auto f: net->flows) {
		if (f->slink->Full() || f->dlink->Full())
			continue;
		f->r = Rate(FLAGS_line_rate);
		f->allocated += f->r;
		f->slink->Consume(f->r);
		f->dlink->Consume(f->r);
	}
}

void CoFlowSRPTAllocator::ComputeRates() {
	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows) {
		flow->Reset();
	}

	sort(net->flows.begin(), net->flows.end(), CompareCoFlowRemainingSizes);

	for (auto f: net->flows) {
		if (f->slink->Full() || f->dlink->Full())
			continue;
		f->r = Rate(FLAGS_line_rate);
		f->allocated += f->r;
		f->slink->Consume(f->r);
		f->dlink->Consume(f->r);
	}
}

void CoFlowSizePrioAllocator::ComputeRates() {
	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows)
		flow->Reset();

	sort(net->flows.begin(), net->flows.end(), CompareCoFlowSizes);

	for (auto f: net->flows) {
		if (f->slink->Full() || f->dlink->Full())
			continue;
		f->r = Rate(FLAGS_line_rate);
		f->allocated += f->r;
		f->slink->Consume(f->r);
		f->dlink->Consume(f->r);
	}
}

void FIFOAllocator::ComputeRates() {
	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows) {
		flow->Reset();
	}

	for (auto f: net->flows) {
		if (f->slink->Full() || f->dlink->Full())
			continue;
		f->r = Rate(FLAGS_line_rate);
		f->allocated += f->r;
		f->slink->Consume(f->r);
		f->dlink->Consume(f->r);
	}
}

void PerTenantFairAllocator::ComputeRates() {
	int num_allocated = 0;
	list<Flow *> lflows;

	for (auto link: net->links) {
		link->ResetActive();
	}

	for (auto flow: net->flows) {
		flow->Reset();
		lflows.push_back(flow);
	}

	while (num_allocated < net->flows.size()) {
		for (auto f: lflows) {
			if (f->Done())
				continue;

			Rate r = min(f->slink->GetAvailable()/NUM_CLASS / f->slink->NumActiveFlows(f->klass),
				     f->dlink->GetAvailable()/NUM_CLASS / f->dlink->NumActiveFlows(f->klass));

			f->r = r;
			f->allocated += r;
			//printf("Flow %p allocated %.3f\n", f, r.Float());
			if (r.Float() < -1) {
				printf("Invalid rate.  Looping...\n");
				while(1);
			}
		}

		for (auto f: lflows) {
			if (f->Done())
				continue;

			f->slink->Consume(f->r);
			f->dlink->Consume(f->r);
		}

		for (auto it = lflows.begin(); ; ) {
			if (it == lflows.end())
				break;

			Flow *f = *it;
			if (f->Done()) {
				/* This shouldn't happen */
				it = lflows.erase(it);
				continue;
			}

			if (f->slink->Full() || f->dlink->Full()) {
				f->slink->DecActive(f->klass);
				f->dlink->DecActive(f->klass);
				num_allocated++;
				f->Bottlenecked();
				it = lflows.erase(it);
			} else {
				it++;
			}
		}
	}
}

void PerTenantSRPTAllocator::ComputeRates() {
	int num_allocated = 0;
	bool done;
	vector<Flow *> flows[MAX_NUM_CLASS];

	for (auto link: net->links) {
		link->ResetActive();
		REP(i, NUM_CLASS)
			link->SetCapacity(i, link->GetAvailable()/NUM_CLASS);
	}

	for (auto flow: net->flows) {
		flow->Reset();
		assert(flow->klass != -1);
		flows[flow->klass].push_back(flow);
	}

	REP(i, NUM_CLASS)
		sort(flows[i].begin(), flows[i].end(), CompareFlows);

	u32 loop = 0;
	do {
		done = true;
		loop++;
		//CHECK_LE(loop, 1000) << "Potential looping in scheduler";
		if (loop >= 1000) {
			break;
		}

		REP(i, NUM_CLASS) {
			for (auto f: flows[i]) {
				if (f->slink->Full(f->klass) || f->dlink->Full(f->klass))
					continue;

				done = false;
				Rate r = min(f->slink->GetAvailable(i),
					     f->dlink->GetAvailable(i));

				f->r = r;
				f->allocated += f->r;
				f->slink->Consume(f->r, f->klass);
				f->dlink->Consume(f->r, f->klass);
			}
		}

		for (auto l: net->links) {
			if (l->Full())
				continue;
			Rate r = l->GetAvailable() / NUM_CLASS;
			REP(i, NUM_CLASS)
				l->AddCapacity(i, r);
		}
	} while (!done);
}

void PerTenantSRPTFairAllocator::ComputeRates() {
	int num_allocated = 0;
	bool done;
	vector<Flow *> flows[MAX_NUM_CLASS];

	for (auto link: net->links) {
		link->ResetActive();
		REP(i, NUM_CLASS)
			link->SetCapacity(i, link->GetAvailable()/NUM_CLASS);
	}

	for (auto flow: net->flows) {
		flow->Reset();
		assert(flow->klass != -1);
		flows[flow->klass].push_back(flow);
	}

	REP(i, NUM_CLASS)
		sort(flows[i].begin(), flows[i].end(), CompareFlows);

	do {
		done = true;
		REP(i, NUM_CLASS) {
			for (auto f: flows[i]) {
				if (f->slink->Full(f->klass) || f->dlink->Full(f->klass))
					continue;
				if (f->Done())
					continue;

				done = false;
				/* Arbitrary */
				if (i == 0) {
					Rate r = min(f->slink->GetAvailable(i),
						     f->dlink->GetAvailable(i));
					f->r = r;
				} else {
					Rate r = min(f->slink->GetAvailable(i) / f->slink->NumActiveFlows(f->klass),
						     f->dlink->GetAvailable(i) / f->dlink->NumActiveFlows(f->klass));
					f->r = r;
				}

				f->allocated += f->r;
				f->slink->Consume(f->r, f->klass);
				f->dlink->Consume(f->r, f->klass);
			}
		}

		/* Redistribute remaining capacity */
		for (auto l: net->links) {
			if (l->Full())
				continue;
			Rate r = l->GetAvailable() / NUM_CLASS;
			REP(i, NUM_CLASS)
				l->AddCapacity(i, r);
		}

		/* For klass=1, decrement active flows; not needed,
		 * but speeds things up */
		for (auto it = flows[1].begin(); it != flows[1].end(); it++) {
			Flow *f = *it;
			if (f->Done()) {
				continue;
			}

			if (f->slink->Full(f->klass) || f->dlink->Full(f->klass)) {
				f->slink->DecActive(f->klass);
				f->dlink->DecActive(f->klass);
				num_allocated++;
				f->Bottlenecked();
			}
		}

	} while (!done);
}

#endif /* __ALLOCATOR_CC__ */
