#ifndef __NET_H__
#define __NET_H__

struct Allocator;
struct App;
struct Flow;
struct Link;

#include "rate_est.hh"

struct Net
{
	vector<Flow *> flows;
	vector<Link *> links;
	vector<App *> apps;
	Allocator *allocator;
	RateEstimator txrate[MAX_NUM_NODES];
	RateEstimator rxrate[MAX_NUM_NODES];

	int num_nodes;
	int total_flows;

	Net() {total_flows = 0;}
	void CreateTopology(int);
	void AddApp(App *app);
	Flow *PickEarliest(TimeUnit t);
	TimeUnit NextEarliestArrival();
	TimeUnit ComputeFinishTimes();
	void DrainFlows(TimeUnit now, TimeUnit dt);
	void FlushFlows(TimeUnit now);
	void ComputeRates();
	int TotalFlows() { return total_flows; }
	void AddFlow(Flow *f);
	void NotifyFlowCompletion(Flow*, TimeUnit when);
};

#endif /* __NET_H__ */
