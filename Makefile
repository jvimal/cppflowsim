
LIBS=-L/usr/local/lib -lglog -lgflags
FLAGS=-O3
CXX=clang++ -std=c++0x -stdlib=libc++ -I/usr/local/include
# CXX=clang++ -std=c++0x -I/usr/include/c++/4.6.2 -I/usr/include/c++/4.6.2/x86_64-linux-gnu/

all:
	$(CXX) $(FLAGS) $(LIBS) sched.cpp -o sched

