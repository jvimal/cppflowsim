#!/bin/env python

import argparse
import logging
import os
import datetime
import subprocess
import itertools
import time
import multiprocessing

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', default='out/' + datetime.datetime.now().strftime('%b%d-%H-%M'),)
parser.add_argument('--sched', default='./sched')
parser.add_argument('--target_load', type=float, default=0.5)
FLAGS = parser.parse_args()

allocators = ['pff', 'ptf', 'fsrpt', 'csrpt', 'fsizep', 'csizep']
bursts = [1, 2, 4]
TARGET_LOAD=FLAGS.target_load
loads = [0.1, 0.3, 0.5, 0.7, 0.8]
MEM_LIMIT='1000000' # 1GB limit
MAX_PROCS=max(1, multiprocessing.cpu_count()-1)
MAX_FLOWS=1000000

scenarios = itertools.product(allocators, bursts, loads)
procs = []
doneprocs = []

def dir(idx, alloc,burst,load):
    return "%03d-alloc-%s--burst-%s--load-%s" % (idx, alloc, burst, load)

def start(alloc,burst,load,outfile):
    cmd = "%s -allocator %s -max_flows %s -burst_flow_arrivals %s -loads %s,%s -num_apps 2"
    cmd = cmd % (FLAGS.sched, alloc, MAX_FLOWS, burst, load, FLAGS.target_load-load)
    cmd = "(%s >%s 2>&1)" % (cmd, outfile)
    limit = "ulimit -Sv %s" % MEM_LIMIT
    proc = subprocess.Popen("%s; %s;" % (limit, cmd), shell=True)
    logging.info("Started %s" % cmd)
    procs.append(proc)

    f = open(os.path.dirname(outfile) + "/cmd", 'w')
    f.write(cmd)
    f.close()
    return proc

def wait(all=False):
    global procs, doneprocs
    while True:
        if all is False and len(procs) != MAX_PROCS:
            break
        if all is True and len(procs) == 0:
            break
        time.sleep(1)
        keep = []
        for p in procs:
            if p.poll() is None:
                keep.append(p)
            else:
                doneprocs.append(p)
        procs = keep
    return

if not os.path.exists(FLAGS.dir):
    os.makedirs(FLAGS.dir)

idx = -1
scenarios = list(scenarios)
scenarios = filter(lambda (a,b,load): FLAGS.target_load > load, scenarios)

for alloc,burst,load in scenarios:
    idx += 1
    wait()
    exptdir = dir(idx, alloc,burst,load)
    outdir = os.path.join(FLAGS.dir, exptdir)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    outfile = os.path.join(outdir, "log")
    start(alloc,burst,load,outfile)
    N = len(scenarios)
    M = len(doneprocs)
    logging.info("Done %d/%d [%.2f%%]" % (M, N, M*100.0/N))

wait(all=True)
